import { Sequelize } from 'sequelize-typescript';

export const DatabaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'fwd',
      });
      // sequelize.addModels();
      await sequelize.sync();
      return sequelize;
    },
  },
];