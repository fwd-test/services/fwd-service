import { Test, TestingModule } from '@nestjs/testing';
import { FwdController } from './fwd.controller';

describe('FwdController', () => {
  let controller: FwdController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FwdController],
    }).compile();

    controller = module.get<FwdController>(FwdController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
