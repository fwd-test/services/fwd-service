import { Test, TestingModule } from '@nestjs/testing';
import { FwdService } from './fwd.service';

describe('FwdService', () => {
  let service: FwdService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FwdService],
    }).compile();

    service = module.get<FwdService>(FwdService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
