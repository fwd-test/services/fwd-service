import { Body, Controller, Post, Res } from '@nestjs/common';
import { ProductDto } from './dto/product.dto';
import {FwdService} from './fwd.service';
import { Response } from 'express';

@Controller('fwd')
export class FwdController {
  constructor(private fwdService: FwdService) {}
  @Post('getProduct')
  async product(@Body() productDto: ProductDto,  @Res() res: Response) : Promise<Response>{
    const response = await this.fwdService.getProduct(productDto).toPromise()
    return res.status(response.status).json(response.data);
  }
}

