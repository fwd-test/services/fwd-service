export class ProductDto{
  genderCd:string;
  dob : string;
  planCode: string;
  premiumPerYear: number;
  paymentFrequency: string;
  saPerYear: number;
}