import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ProductDto } from './dto/product.dto';
import {AxiosResponse} from 'axios';
import {Observable} from 'rxjs';

@Injectable()
export class FwdService {
  constructor(private httpService: HttpService) {}

  getProduct(product:ProductDto) : Observable<AxiosResponse>{
    const baseurl = 'https://api.fwd.co.th/dev-ecommerce/getProduct';
    return this.httpService.post(baseurl,product);
  }
}
