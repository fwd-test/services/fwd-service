import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FwdModule } from './fwd/fwd.module';

@Module({
  imports: [FwdModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
